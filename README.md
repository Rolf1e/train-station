###### **Train Station Simulator**

This project is being carried out as part of our third year undergraduate training.

The objective of this project is to build a multimodal transport simulator.

A plan of the different means of transport, as well as a json containing information on these means of transport were provided.

The time of passage of each vehicle on a line is calculated.

The entire project is accompanied by a Python script to "visualize" the proper functioning of the simulator.

The simulator manages metro and bus lines.

Here are the lines to use to launch the python in train-station /:
    - python src / python / setup.py install
    - python src / python / start.py

Here is the script to use to launch the simulation, the unit tests and generate the documentation:
    - cd src
    - cmake -U "MakeFiles"
    - make
    - ./main_train_station.out
    - ./main_test.out
    - ./doc

The application is managed by a LineManager.

The LineManager takes a vehicle list and puts it in a priority queue which takes care of designating the next vehicle that needs action.

This vehicle list is created using the JsonParser which retrieves all the information from the supplied json files.

The documentation can be found at the following address:
