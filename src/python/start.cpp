#include <pybind11/pybind11.h>
#include <pybind11/stl.h>
#include <vector>
#include "../main/dto/vehicle/Vehicle.hpp"
#include "../main/dto/User.hpp"
#include "../main/infra/BinaryStation.hpp"
#include "../main/dto/Connection.hpp"
#include "../main/dto/station/Station.hpp"
#include "../main/dto/station/SubwayStation.hpp"
#include "../main/dto/line/Line.hpp"
#include "../main/dto/line/SubwayLine.hpp"
#include "../main/infra/manager/LineManager.hpp"
#include "../main/infra/parser/JsonParser.hpp"
#include "../main/dto/line/BusLine.hpp"
#include "../main/dto/station/BusStation.hpp"
#include "../main/dto/vehicle/Bus.hpp"
#include "../main/infra/manager/PythonManager.hpp"



typedef std::tuple<int, std::vector<std::tuple<std::string, int>>, std::vector<std::tuple<std::string, int>> , std::vector<std::tuple<std::string,std::vector<std::tuple<std::string,int>>>>> pythonData;

std::vector<pythonData> startSimulation() {
    std::string path = __FILE__;
    auto lines = JsonParser::getNetworking("yourkata.json");

    std::cout <<"Veuillez entrer une durée de simulation (en secondes) : "<<std::endl;
    int time;
    
    std::cin>>time;

    LineManager manager(time, lines);

    manager.load();

    manager.start();

    return manager.getPythonData();
}

PYBIND11_PLUGIN(startSimulation) {
    pybind11::module m("startSimulation");
    m.def("startSimulation", &startSimulation);
    return m.ptr();
}
