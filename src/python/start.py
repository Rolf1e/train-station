import startSimulation

data = startSimulation.startSimulation()



print("\n\n ######################################## \n")
print "Nombre de trains sur les lignes : ", data[0][0]
print("\n ######################################## \n")

print("\n\n ######################################## \n")
print "Nombre de bus sur les lignes : ", data[1][0]
print("\n ######################################## \n")




print("\n\n ######################################## \n")
print(" Temps de parcours de chaques rames sur sa ligne : ")
print("\n ######################################## \n")
for id, time in data[0][1]:
    print "la rame ", id , " a circule : " , time , " secondes sur sa ligne"


print("\n\n ######################################## \n")
print(" Temps de parcours de chaques bus sur sa ligne : ")
print("\n ######################################## \n")
for id, time in data[1][1]:
    print "le bus  ", id , " a circule : " , time , " secondes sur sa ligne"




print("\n\n ######################################## \n")
print("Temps entre deux trains a chaques stations")
print("\n ######################################## \n")
for station, time in data[0][2]:
    print "temps entre deux trains a la station ", station , "  : " , time , " secondes"


print("\n\n ######################################## \n")
print("temps entre deux bus a chaques stations")
print("\n ######################################## \n")
for station, time in data[1][2]:
    print "temps entre deux bus a la station ", station , "  : " , time , " secondes"
print("\n ######################################## \n")




print("\n ######################################## \n")
print("horraires de passage des trains pour chaques stations")
print("\n ######################################## \n")

for station in data[0][3]:
    print "Passage des rames en station : " , station[0]
    for rames, time in station[1]:
        print "passage de la rame :  ", rames , "  a " , time , " secondes"
print("\n ######################################## \n")

print("\n ######################################## \n")
print("horraires de passage des bus pour chaques stations")
print("\n ######################################## \n")
for station in data[1][3]:
    print "Passage des bus en station : " , station[0]
    for rames, time in station[1]:
        print "passage du bus :  ", rames , "  a " , time , " secondes"
print("\n ######################################## \n")


