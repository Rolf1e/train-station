from setuptools import setup, Extension

exts = [Extension('startSimulation', sources=[
    'src/python/start.cpp', 'src/main/dto/station/SubwayStation.cpp', 'src/main/infra/manager/LineManager.cpp',
    'src/main/dto/Connection.cpp', 'src/main/dto/vehicle/Subway.cpp', 'src/main/dto/vehicle/VehicleImpl.cpp',
    'src/main/dto/line/SubwayLine.cpp', 'src/main/dto/line/LineImpl.cpp', 'src/main/dto/vehicle/Vehicle.cpp',
    'src/main/dto/station/StationImpl.cpp', 'src/main/infra/BinaryStation.cpp', 'src/main/infra/parser/JsonParser.cpp',
    'src/main/dto/vehicle/Bus.cpp', 'src/main/dto/station/BusStation.cpp', 'src/main/dto/line/BusLine.cpp',
    'src/main/infra/manager/PythonManager.cpp',
    'src/main/infra/BinarySeeker.cpp'])]

setup(name='py_start',
      version='0.1.0',
      ext_modules=exts,
      scripts=['src/python/start.py'],
      python_requires='>=3',
      install_requires=[])
