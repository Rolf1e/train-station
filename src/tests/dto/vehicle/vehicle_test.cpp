#include <CppUTest/CommandLineTestRunner.h>
#include "../../../main/dto/vehicle/Vehicle.hpp"
#include "../../../main/dto/vehicle/Subway.hpp"

TEST_GROUP(GroupVehicle) {
};

TEST(GroupVehicle, TestConstuctor) {

    std::vector<std::shared_ptr<User>> users = {};

    std::shared_ptr<Vehicle> v = std::make_shared<Subway>(1, 10, UP, STOP, users, "L1");
    v->setStationId("S1");

    CHECK_EQUAL(v->getDirection(), UP);
    CHECK_EQUAL(v->getLineId() == "L1", true)
    CHECK_EQUAL(v->getStationId() == "S1", true)
}
