
#include <CppUTest/CommandLineTestRunner.h>
#include "../../../main/dto/vehicle/Subway.hpp"
#include "../../../main/dto/station/Station.hpp"
#include "../../../main/dto/station/SubwayStation.hpp"
#include "../../../main/dto/line/Line.hpp"
#include "../../../main/dto/line/SubwayLine.hpp"

TEST_GROUP(GroupLine) {
};

TEST(GroupLine, TestConstuctor) {

    std::vector<std::shared_ptr<User>> users;
    std::vector<std::shared_ptr<Vehicle>> vehicles = {std::make_shared<Subway>(1, 10, UP, STOP, users, "L1")};

    BinaryStation binaryStationS1S2("S1", 0, "S2", 20);
    Connection connectionS1S2(binaryStationS1S2);

    BinaryStation binaryStationS1S3("S1", 20, "S3", 20);
    Connection connectionS1S3(binaryStationS1S3);

    BinaryStation binaryStationS3S2("S2", 20, "S3", 0);
    Connection connectionS2S3(binaryStationS3S2);

    std::vector<std::shared_ptr<Station>> stations = {
            std::make_shared<SubwayStation>("S1", connectionS1S2, users),
            std::make_shared<SubwayStation>("S2", connectionS1S3, users),
            std::make_shared<SubwayStation>("S3", connectionS2S3, users)
    };

    std::shared_ptr<Line> line = std::make_shared<SubwayLine>("L1", 20, vehicles, stations);

    CHECK_EQUAL(line->getId() == "L1", true);
    CHECK_EQUAL(line->getStations().size(), stations.size());
    CHECK_EQUAL(line->getFlipDuration(), 20);
    CHECK_EQUAL(line->getVehicles().size(), vehicles.size());
}
