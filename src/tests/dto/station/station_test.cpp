#include <CppUTest/CommandLineTestRunner.h>
#include "../../../main/dto/vehicle/Subway.hpp"
#include "../../../main/dto/station/Station.hpp"
#include "../../../main/dto/station/SubwayStation.hpp"

TEST_GROUP(GroupStation) {
};

TEST(GroupStation, TestConstuctor) {
    std::vector<std::shared_ptr<User>> users = {};
    BinaryStation binaryStationS1S2("S1", 0, "S2", 20);
    Connection connectionS1S2(binaryStationS1S2);

    std::shared_ptr<Station> s = std::make_shared<SubwayStation>("S1", binaryStationS1S2, users);

    CHECK_EQUAL(s->getId() == "S1", true);
    Connection connection = s->getNeighbours();
    CHECK_EQUAL(connection.getNeighbours().getTimeUp(), 0);
    CHECK_EQUAL(connection.getNeighbours().getTimeDown(), 20);
}
