#include <CppUTest/CommandLineTestRunner.h>
#include <vector>
#include <iostream>
#include "../../../main/dto/vehicle/Vehicle.hpp"
#include "../../../main/dto/User.hpp"
#include "../../../main/infra/BinaryStation.hpp"
#include "../../../main/dto/Connection.hpp"
#include "../../../main/dto/station/Station.hpp"
#include "../../../main/dto/station/SubwayStation.hpp"
#include "../../../main/dto/line/Line.hpp"
#include "../../../main/dto/line/SubwayLine.hpp"
#include "../../../main/infra/manager/LineManager.hpp"

TEST_GROUP(GroupLineManager) {
};

TEST(GroupLineManager, TestConstuctor) {

    std::vector<std::shared_ptr<User>> users;

    const std::shared_ptr<Subway> &vehicle = std::make_shared<Subway>(20, 20, UP, STOP, users, "L1");
    vehicle->setStationId("S1");
    vehicle->setType("train");
    vehicle->setId("t1");
    const std::shared_ptr<Subway> &vehicle2 = std::make_shared<Subway>(0, 0, UP, STOP, users, "L1");
    vehicle2->setStationId("S2");
    vehicle2->setType("train");
    vehicle2->setId("t2");

    const std::shared_ptr<Subway> &vehicle3 = std::make_shared<Subway>(10, 10, UP, STOP, users, "L2");
    vehicle3->setStationId("S1");
    vehicle3->setType("train");
    vehicle3->setId("t3");
    const std::shared_ptr<Subway> &vehicle4 = std::make_shared<Subway>(30, 30, UP, STOP, users, "L2");
    vehicle4->setStationId("S2");
    vehicle4->setType("train");
    vehicle4->setId("t4");

    std::vector<std::shared_ptr<Vehicle>> vehicles{
            vehicle, vehicle2, vehicle3, vehicle4
    };

    BinaryStation binaryStationS1S2("S1", 0, "S2", 20);
    Connection connectionS1S2(binaryStationS1S2);

    BinaryStation binaryStationS1S3("S1", 20, "S3", 20);
    Connection connectionS1S3(binaryStationS1S3);

    BinaryStation binaryStationS3S2("S2", 20, "S3", 0);
    Connection connectionS2S3(binaryStationS3S2);

    std::vector<std::shared_ptr<Station>> stations = {
            std::make_shared<SubwayStation>("S1", connectionS1S2, users),
            std::make_shared<SubwayStation>("S2", connectionS1S3, users),
            std::make_shared<SubwayStation>("S3", connectionS2S3, users)
    };

    std::vector<std::shared_ptr<Line>> lines = {
            std::make_shared<SubwayLine>("L1", 20, vehicles, stations),
            std::make_shared<SubwayLine>("L2", 10, vehicles, stations)
    };

    LineManager manager(1000, lines);

    CHECK_EQUAL(lines.empty(), false);
}
