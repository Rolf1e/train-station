//
// Created by arthur on 12/06/2020.
//

#include <vector>
#include "../../../main/dto/line/Line.hpp"
#include "../../../main/infra/parser/JsonParser.hpp"
#include <CppUTest/CommandLineTestRunner.h>
#include <iostream>

TEST_GROUP(GroupJsonParser) {
    
    static std::string path()
    {
        std::string path = __FILE__;
        return  path.substr(0, path.size() - 42) + "yourkata.json";
    }

};

TEST(GroupJsonParser, TestSize) {

    std::vector<std::shared_ptr<Line>>  lines = JsonParser::getNetworking(path()  );
    CHECK_EQUAL(lines.size(), 12);

}


TEST(GroupJsonParser, TestLineName) {


    std::vector<std::shared_ptr<Line>>  lines = JsonParser::getNetworking(path()  );
    CHECK_EQUAL(lines[0].get()->getId(), "L1");

}

TEST(GroupJsonParser, TestLineFlipDuration) {


    std::vector<std::shared_ptr<Line>>  lines = JsonParser::getNetworking(path()  );
    CHECK_EQUAL(lines[0].get()->getFlipDuration(), 240);

}

TEST(GroupJsonParser, TestVehicleDirection) {

    std::vector<std::shared_ptr<Line>>  lines = JsonParser::getNetworking(path()  );
    CHECK_EQUAL(lines[0].get()->getVehicles()[0]->getDirection(), UP);

}


TEST(GroupJsonParser, TestVehicleLine) {

    std::vector<std::shared_ptr<Line>>  lines = JsonParser::getNetworking(path()  );
    CHECK_EQUAL(lines[0].get()->getVehicles()[0]->getLineId(), "L1");

}

TEST(GroupJsonParser, TestVehicleSize) {

    std::vector<std::shared_ptr<Line>>  lines = JsonParser::getNetworking(path()  );
    CHECK_EQUAL(lines[0].get()->getVehicles().size(), 8);

}

TEST(GroupJsonParser, TestStationSize) {

    std::vector<std::shared_ptr<Line>>  lines = JsonParser::getNetworking(path()  );
    CHECK_EQUAL(lines[0].get()->getStations().size(), 6);

}