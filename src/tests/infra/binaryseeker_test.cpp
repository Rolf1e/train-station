#include <CppUTest/CommandLineTestRunner.h>

#include "../../main/infra/BinarySeeker.hpp"
#include "../../main/dto/line/SubwayLine.hpp"
#include "../../main/dto/station/SubwayStation.hpp"
#include "../../main/dto/vehicle/Subway.hpp"

TEST_GROUP(GroupBinarySeeker) {
};

TEST(GroupBinarySeeker, searchStation) {

    std::vector<std::shared_ptr<User>> users;

    const std::shared_ptr<Subway> &vehicle = std::make_shared<Subway>(1, 10, UP, STOP, users, "L1");
    vehicle->setStationId("S1");
    std::vector<std::shared_ptr<Vehicle>> vehicles = {
            vehicle
    };

    BinaryStation binaryStationS1S2("S1", 0, "S2", 20);
    Connection connectionS1S2(binaryStationS1S2);

    BinaryStation binaryStationS1S3("S1", 20, "S3", 20);
    Connection connectionS1S3(binaryStationS1S3);

    BinaryStation binaryStationS3S2("S2", 20, "S3", 0);
    Connection connectionS2S3(binaryStationS3S2);

    std::vector<std::shared_ptr<Station>> stations = {
            std::make_shared<SubwayStation>("S1", connectionS1S2, users),
            std::make_shared<SubwayStation>("S2", connectionS1S3, users),
            std::make_shared<SubwayStation>("S3", connectionS2S3, users)
    };


    const auto &line = std::make_shared<SubwayLine>("L1", 20, vehicles, stations);

    const BinaryStation &station = BinarySeeker::searchBinaryInStations(line, vehicle);

   CHECK_EQUAL(station.getStationUp() == binaryStationS1S2.getStationUp(), true);
}