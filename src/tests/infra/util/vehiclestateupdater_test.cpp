#include <CppUTest/CommandLineTestRunner.h>

#include "../../../main/infra/util/VehicleStateUpdater.hpp"
#include "../../../main/dto/vehicle/Vehicle.hpp"
#include "../../../main/dto/vehicle/State.hpp"
#include "../../../main/dto/vehicle/Subway.hpp"
#include "../../../main/dto/User.hpp"

TEST_GROUP(GroupVehicleStateUpdater) {

};

TEST(GroupVehicleStateUpdater, start) {
	std::vector<std::shared_ptr<User>> users;
	const auto &vehicle = std::make_shared<Subway>(20, 20, UP, STOP, users, "L1");

	const auto started = VehicleStateUpdater::start(vehicle, 1);
	CHECK_EQUAL(started->getState(), START);
}

TEST(GroupVehicleStateUpdater, stop) {
	std::vector<std::shared_ptr<User>> users;
	const auto &vehicle = std::make_shared<Subway>(20, 20, UP, STOP, users, "L1");

	const auto stoped = VehicleStateUpdater::stop(vehicle, 1);
	CHECK_EQUAL(stoped->getState(), STOP);
}

TEST(GroupVehicleStateUpdater, pending) {
	std::vector<std::shared_ptr<User>> users;
	const auto &vehicle = std::make_shared<Subway>(20, 20, UP, STOP, users, "L1");

	//That is awful to test, let me come back later //TODO
	//const auto pending = VehicleStateUpdater::pending(vehicle, 1);
	//CHECK_EQUAL(pending->getState(), PENDING);
}

TEST(GroupVehicleStateUpdater, flip) {
	std::vector<std::shared_ptr<User>> users;
	const auto &vehicle = std::make_shared<Subway>(20, 20, UP, STOP, users, "L1");

	const auto flip = VehicleStateUpdater::flip(vehicle, 1);
	CHECK_EQUAL(flip->getState(), FLIP);
}

TEST(GroupVehicleStateUpdater, running) {
	std::vector<std::shared_ptr<User>> users;
	const auto &vehicle = std::make_shared<Subway>(20, 20, UP, STOP, users, "L1");

	const auto running = VehicleStateUpdater::running(vehicle);
	CHECK_EQUAL(running->getState(), RUNNING);
}

