#include <CppUTest/CommandLineTestRunner.h>
#include "../../../main/infra/util/EnumConverter.hpp"

TEST_GROUP(GroupEnumConverter) {
};

TEST(GroupEnumConverter, getStringState) {
	
	STRCMP_EQUAL(EnumConverter::getStringState(0).c_str()			, "STOP");
	STRCMP_EQUAL(EnumConverter::getStringState(1).c_str()			, "START");
	STRCMP_EQUAL(EnumConverter::getStringState(2).c_str()			, "RUNNING");
	STRCMP_EQUAL(EnumConverter::getStringState(3).c_str()			, "FLIP");
	STRCMP_EQUAL(EnumConverter::getStringState(4).c_str()			, "PENDING");
	STRCMP_EQUAL(EnumConverter::getStringState(7).c_str()			, "UNKNOWN");
}


TEST(GroupEnumConverter, getStringDirection) {
	
	STRCMP_EQUAL(EnumConverter::getStringDirection(0).c_str()			, "DOWN");
	STRCMP_EQUAL(EnumConverter::getStringDirection(1).c_str()			, "UP");
	STRCMP_EQUAL(EnumConverter::getStringDirection(3).c_str()			, "UNKNOWN");

}
