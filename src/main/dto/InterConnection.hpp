//
// Created by arthur on 29/06/2020.
//

#ifndef TRAIN_STATION_INTERCONNECTION_H
#define TRAIN_STATION_INTERCONNECTION_H


#include <tuple>
#include <string>

class InterConnection {
public:
    const std::tuple<std::string, std::string> &getFirstLine() const;

    const std::tuple<std::string, std::string> &getSecondLine() const;

private:
    std::tuple<std::string, std::string> _firstLine;
    std::tuple<std::string, std::string> _secondLine;
    int _duration;


public:
    InterConnection(const std::tuple<std::string, std::string> &subway, const std::tuple<std::string, std::string> &bus,
                    int duration);

    const std::tuple <std::string, std::string> &getSubway() const;

    const std::tuple <std::string, std::string> &getBus() const;

    int getDuration() const;
};


#endif //TRAIN_STATION_INTERCONNECTION_H
