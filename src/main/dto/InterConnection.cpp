//
// Created by arthur on 29/06/2020.
//

#include "InterConnection.hpp"



int InterConnection::getDuration() const {
    return _duration;
}

InterConnection::InterConnection(const std::tuple<std::string, std::string> &firstLine,
                                 const std::tuple<std::string, std::string> &secondLine, int duration) : _firstLine(firstLine),
                                                                                                  _secondLine(secondLine),
                                                                                                  _duration(duration) {}

const std::tuple<std::string, std::string> &InterConnection::getFirstLine() const {
    return _firstLine;
}

const std::tuple<std::string, std::string> &InterConnection::getSecondLine() const {
    return _secondLine;
}
