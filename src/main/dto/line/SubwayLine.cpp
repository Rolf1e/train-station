#include "SubwayLine.hpp"

SubwayLine::SubwayLine(const std::string &id, int flipDuration, const std::vector<std::shared_ptr<Vehicle>> &vehicles,
                       const std::vector<std::shared_ptr<Station>> &stations) : LineImpl(id, flipDuration, vehicles,
                                                                                         stations) {}
