#ifndef TRAIN_STATION_SUBWAYLINE_HPP
#define TRAIN_STATION_SUBWAYLINE_HPP


#include "LineImpl.hpp"

class SubwayLine : public LineImpl {
public:
    SubwayLine(const std::string &id, int flipDuration, const std::vector<std::shared_ptr<Vehicle>> &vehicles,
               const std::vector<std::shared_ptr<Station>> &stations);



};


#endif //TRAIN_STATION_SUBWAYLINE_HPP
