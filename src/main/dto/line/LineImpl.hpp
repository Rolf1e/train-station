#ifndef TRAIN_STATION_LINEIMPL_HPP
#define TRAIN_STATION_LINEIMPL_HPP


#include <string>
#include <vector>
#include "../vehicle/Vehicle.hpp"
#include "../station/Station.hpp"
#include "Line.hpp"

class LineImpl : public Line {
private:
    std::string _id;
    int _flipDuration;
    std::vector<std::shared_ptr<Vehicle>> _vehicles;
    std::vector<std::shared_ptr<Station>> _stations;

protected:
public:
    LineImpl(const std::string &id, int flipDuration, const std::vector<std::shared_ptr<Vehicle>> &vehicles,
             const std::vector<std::shared_ptr<Station>> &stations);

public:
    const std::vector<std::shared_ptr<Station>> &getStations() const override;

    const int &getFlipDuration() const override;

    const std::vector<std::shared_ptr<Vehicle>> &getVehicles() const override;

    void addStation(const std::shared_ptr<Station> &station) override;

    void addVehicle(const std::shared_ptr<Vehicle> &station) override;

    const std::string getId() const override;
};


#endif //TRAIN_STATION_LINEIMPL_HPP
