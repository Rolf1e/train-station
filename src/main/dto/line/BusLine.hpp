#ifndef TRAIN_STATION_BUSLINE_HPP
#define TRAIN_STATION_BUSLINE_HPP


#include "LineImpl.hpp"

class BusLine : public LineImpl {
public:
    BusLine(const std::string &id, int flipDuration, const std::vector<std::shared_ptr<Vehicle>> &vehicles,
               const std::vector<std::shared_ptr<Station>> &stations);



};


#endif //TRAIN_STATION_BUSLINE_HPP
