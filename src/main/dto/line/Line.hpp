#ifndef TRAIN_STATION_LINE_HPP
#define TRAIN_STATION_LINE_HPP

#include "../station/Station.hpp"
#include "../vehicle/Vehicle.hpp"
#include <vector>
#include <memory>

class Line {
public:
    virtual const int &getFlipDuration() const = 0;

    virtual const std::vector<std::shared_ptr<Station>> &getStations() const = 0;

    virtual const std::vector<std::shared_ptr<Vehicle>> &getVehicles() const = 0;

    virtual void addStation(const std::shared_ptr<Station> &station) = 0;

    virtual void addVehicle(const std::shared_ptr<Vehicle> &vehicle) = 0;

    virtual const std::string getId() const = 0;
};


#endif //TRAIN_STATION_LINE_HPP
