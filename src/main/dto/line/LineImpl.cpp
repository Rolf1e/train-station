#include <memory>
#include "LineImpl.hpp"

const std::string LineImpl::getId() const {
    return _id;
}

const std::vector<std::shared_ptr<Station>> &LineImpl::getStations() const {
    return _stations;
}

const int &LineImpl::getFlipDuration() const {
    return _flipDuration;
}

const std::vector<std::shared_ptr<Vehicle>> &LineImpl::getVehicles() const {
    return _vehicles;
}


void LineImpl::addStation(const std::shared_ptr<Station> &station) {
    _stations.push_back(station);
}

void LineImpl::addVehicle(const std::shared_ptr<Vehicle> &vehicle) {
    _vehicles.push_back(vehicle);
}


LineImpl::LineImpl(const std::string &id, int flipDuration, const std::vector<std::shared_ptr<Vehicle>> &vehicles,
                   const std::vector<std::shared_ptr<Station>> &stations) : _id(id), _flipDuration(flipDuration),
                                                                            _vehicles(vehicles),
                                                                            _stations(stations) {}
