#ifndef TRAIN_STATION_BUS_HPP
#define TRAIN_STATION_BUS_HPP

#include <memory>
#include "../User.hpp"
#include "VehicleImpl.hpp"


class Bus : public VehicleImpl {

public:
    Bus(int startingDate, int endDate, Direction direction, State state,
           const std::vector<std::shared_ptr<User>> &users, const std::string &lineId);

    Bus(const std::string &lineId, const std::string &stationId, int startingDate, int endDate,
           Direction direction, State state, const std::vector<std::shared_ptr<User>> &users);

    Bus(const std::string &lineId, const std::string &stationId, int startingDate, int endDate,
           Direction direction, State state, const std::vector<std::shared_ptr<User>> &users, std::string type);

    Bus(const std::string &lineId, const std::string &stationId, int startingDate, int endDate,
           Direction direction, State state, const std::vector<std::shared_ptr<User>> &users, std::string vehiculeId, std::string type);

    std::shared_ptr<Vehicle> update(const std::string &stationId, const State &state, int time) const override;
};

#endif //TRAIN_STATION_BUS_HPP
