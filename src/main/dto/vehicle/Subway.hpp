#ifndef TRAIN_STATION_SUBWAY_HPP
#define TRAIN_STATION_SUBWAY_HPP

#include <memory>
#include "../User.hpp"
#include "VehicleImpl.hpp"


class Subway : public VehicleImpl {

public:
    Subway(int startingDate, int endDate, Direction direction, State state,
           const std::vector<std::shared_ptr<User>> &users, const std::string &lineId);

    Subway(const std::string &lineId, const std::string &stationId, int startingDate, int endDate,
           Direction direction, State state, const std::vector<std::shared_ptr<User>> &users);

    Subway(const std::string &lineId, const std::string &stationId, int startingDate, int endDate,
           Direction direction, State state, const std::vector<std::shared_ptr<User>> &users, std::string type);

    Subway(const std::string &lineId, const std::string &stationId, int startingDate, int endDate,
           Direction direction, State state, const std::vector<std::shared_ptr<User>> &users, std::string vehiculeId, std::string type);

    std::shared_ptr<Vehicle> update(const std::string &stationId, const State &state, int time) const override;
};

#endif //TRAIN_STATION_SUBWAY_HPP
