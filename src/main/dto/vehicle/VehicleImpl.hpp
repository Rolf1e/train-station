#ifndef TRAIN_STATION_VEHICLEIMPL_HPP
#define TRAIN_STATION_VEHICLEIMPL_HPP

#include "Direction.hpp"
#include "State.hpp"
#include "../User.hpp"
#include "Vehicle.hpp"
#include <vector>
#include <memory>

class VehicleImpl : public Vehicle {
protected:
    std::string _vehicleId;
    std::string _lineId;
    std::string _stationId;
    std::string _type;
    int _startingDate;
    int _endDate;
    Direction _direction;
    State _state;
    std::vector<std::shared_ptr<User>> _users;

protected:
    VehicleImpl(int startingDate, int endDate, Direction direction, State state,
                const std::vector<std::shared_ptr<User>> &users, const std::string &lineId);

    VehicleImpl(const std::string &lineId, const std::string &stationId, int startingDate, int endDate,
                Direction direction, State state, const std::vector<std::shared_ptr<User>> &users);

    VehicleImpl(const std::string &lineId, const std::string &stationId, int startingDate, int endDate,
                Direction direction, State state, const std::vector<std::shared_ptr<User>> &users, std::string type);

    VehicleImpl(const std::string &lineId, const std::string &stationId, int startingDate, int endDate,
                Direction direction, State state, const std::vector<std::shared_ptr<User>> &users, std::string vehiculeId, std::string type);

public:
    void addTime(int time) override;

    void run() override;

    int getStartingDate() const override;

    int getEndDate() const override;

    Direction getDirection() const override;

    State getState() const override;

    const std::vector<std::shared_ptr<User>> &getUsers() const override;

    std::string getLineId() const override;

    std::string getStationId() const override;

    std::string getId() const override;

    void setStationId(const std::string &stationId) override;

    void setState(const State &state) override;

    void setDirection(const Direction &direction) override;

    std::string getType() const override;

    void setType(const std::string &type);

    void setId(const std::string &id);

    std::ostream &operator<<(std::ostream &out) const override;

    bool operator<(const std::shared_ptr<Vehicle> &rhs) const override;
};

#endif //TRAIN_STATION_VEHICLEIMPL_HPP
