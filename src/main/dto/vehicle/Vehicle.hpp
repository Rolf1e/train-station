#ifndef TRAIN_STATION_VEHICLE_HPP
#define TRAIN_STATION_VEHICLE_HPP

#include <memory>
#include <ostream>
#include <vector>
#include "Direction.hpp"
#include "State.hpp"
#include "../User.hpp"


class Vehicle {
protected:
    static int CURRENT_ID;
public:

    virtual void run() = 0;

    virtual void addTime(int time) = 0;

    virtual std::string getLineId() const = 0;

    virtual std::string getStationId() const = 0;

    virtual std::string getId() const = 0;

    virtual Direction getDirection() const = 0;

    virtual int getStartingDate() const = 0;

    virtual int getEndDate() const = 0;

    virtual State getState() const = 0;

    virtual std::string getType() const = 0;

    virtual const std::vector<std::shared_ptr<User>> &getUsers() const = 0;

    virtual void setStationId(const std::string &name) = 0;

    virtual void setState(const State &state) = 0;

    virtual void setDirection(const Direction &direction) = 0;

    virtual std::ostream &operator<<(std::ostream &out) const = 0;

    virtual std::shared_ptr<Vehicle> update(const std::string &stationId, const State &state, int time) const = 0;

    virtual bool operator<(const std::shared_ptr<Vehicle> &rhs) const = 0;
};

std::ostream &operator<<(std::ostream &out, const Vehicle &vehicle);

#endif //TRAIN_STATION_VEHICLE_HPP
