#include <vector>
#include "Subway.hpp"

Subway::Subway(int startingDate, int endDate, Direction direction, State state,
               const std::vector<std::shared_ptr<User>> &users, const std::string &lineId)
        : VehicleImpl(startingDate, endDate, direction, state, users, lineId) {}

Subway::Subway(const std::string &lineId, const std::string &stationId, int startingDate, int endDate,
               Direction direction, State state, const std::vector<std::shared_ptr<User>> &users) : VehicleImpl(lineId,
                                                                                                                stationId,
                                                                                                                startingDate,
                                                                                                                endDate,
                                                                                                                direction,
                                                                                                                state,
                                                                                                                users) {

}

Subway::Subway(const std::string &lineId, const std::string &stationId, int startingDate, int endDate,
               Direction direction, State state, const std::vector<std::shared_ptr<User>> &users, std::string type)
        : VehicleImpl(lineId, stationId, startingDate, endDate,
                      direction, state,
                      users, type) {}

Subway::Subway(const std::string &lineId, const std::string &stationId, int startingDate, int endDate,
               Direction direction, State state, const std::vector<std::shared_ptr<User>> &users, std::string vehiculeId, std::string type)
        : VehicleImpl(lineId, stationId, startingDate, endDate,
                      direction, state,
                      users, vehiculeId, type) {}

std::shared_ptr<Vehicle> Subway::update(const std::string &stationId, const State &state, int time) const {
    int endDate = _endDate;
    return std::make_shared<Subway>(_lineId, stationId, endDate, endDate += time, _direction, state, _users, _vehicleId, _type);
}
