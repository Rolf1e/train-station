#ifndef TRAIN_STATION_DIRECTION_HPP
#define TRAIN_STATION_DIRECTION_HPP

enum Direction {
    DOWN = 0, UP = 1
};

#endif //TRAIN_STATION_DIRECTION_HPP
