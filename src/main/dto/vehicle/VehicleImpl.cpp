#include <memory>
#include "VehicleImpl.hpp"
#include "../../infra/util/EnumConverter.hpp"

VehicleImpl::VehicleImpl(int startingDate, int endDate, Direction direction, State state,
		const std::vector<std::shared_ptr<User>> &users, const std::string &lineId): 
	_lineId(lineId),
	_startingDate(startingDate), 
	_endDate(endDate), 
	_direction(direction), 
	_state(state), 
	_users(users){}


VehicleImpl::VehicleImpl(const std::string &lineId, const std::string &stationId, int startingDate, int endDate,
			Direction direction, State state, const std::vector<std::shared_ptr<User>> &users) : 
	_lineId(lineId), 
	_stationId(stationId), 
	_startingDate(startingDate), 
	_endDate(endDate), 
	_direction(direction),
	_state(state),
	_users(users) {}

VehicleImpl::VehicleImpl(const std::string &lineId, const std::string &stationId, int startingDate, int endDate,
			Direction direction, State state, const std::vector<std::shared_ptr<User>> &users,
			std::string type) : 
	_vehicleId(std::to_string(Vehicle::CURRENT_ID += 1)),
	_lineId(lineId), 
	_stationId(stationId), 
	_type(type),
	_startingDate(startingDate), 
	_endDate(endDate), 
	_direction(direction),
	_state(state),
	_users(users) {}


VehicleImpl::VehicleImpl(const std::string &lineId, const std::string &stationId, int startingDate, int endDate,
			Direction direction, State state, const std::vector<std::shared_ptr<User>> &users, std::string vehiculeId,
			std::string type) :
	_vehicleId(vehiculeId),
	_lineId(lineId),
	_stationId(stationId),
	_type(type), 
	_startingDate(startingDate), 
	_endDate(endDate), 
	_direction(direction),
	_state(state),
	_users(users) {}

void VehicleImpl::addTime(int time) {
	_startingDate = _endDate;
	_endDate += time;
}

void VehicleImpl::run() {
	//TODO
}

int VehicleImpl::getStartingDate() const {
	return _startingDate;
}

int VehicleImpl::getEndDate() const {
	return _endDate;
}

Direction VehicleImpl::getDirection() const {
	return _direction;
}

State VehicleImpl::getState() const {
	return _state;
}

const std::vector<std::shared_ptr<User>> &VehicleImpl::getUsers() const {
	return _users;
}

bool VehicleImpl::operator<(const std::shared_ptr<Vehicle> &rhs) const {
	return _startingDate > rhs->getStartingDate();
}

std::string VehicleImpl::getLineId() const {
	return _lineId;
}

std::string VehicleImpl::getStationId() const {
	return _stationId;
}

std::string VehicleImpl::getId() const {
	return _vehicleId;
}

void VehicleImpl::setStationId(const std::string &stationId) {
	_stationId = stationId;
}

void VehicleImpl::setState(const State &state) {
	_state = state;
}

void VehicleImpl::setDirection(const Direction &direction) {
	_direction = direction;
}

std::string VehicleImpl::getType() const {
	return _type;
}

void VehicleImpl::setType(const std::string &type) {
	_type = type;
}

void VehicleImpl::setId(const std::string &id) {
	_vehicleId = id;
}

std::ostream &
VehicleImpl::operator<<(std::ostream &out) const {
	std::string strState = EnumConverter::getStringState(getState());
	std::string strDirection = EnumConverter::getStringDirection(getDirection());

	out << getType() << " " << getId() << " " << strState << " in station " << getStationId()
		<< " of line "
		<< getLineId() << " in way " << strDirection << std::endl;
	return out;
}


std::ostream &operator<<(std::ostream &out, const Vehicle &vehicle) {
	return vehicle << out;
}
