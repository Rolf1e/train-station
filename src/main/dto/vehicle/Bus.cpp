#include <vector>
#include "Bus.hpp"

Bus::Bus(int startingDate, int endDate, Direction direction, State state,
               const std::vector<std::shared_ptr<User>> &users, const std::string &lineId)
        : VehicleImpl(startingDate, endDate, direction, state, users, lineId) {}

Bus::Bus(const std::string &lineId, const std::string &stationId, int startingDate, int endDate,
               Direction direction, State state, const std::vector<std::shared_ptr<User>> &users) : VehicleImpl(lineId,
                                                                                                                stationId,
                                                                                                                startingDate,
                                                                                                                endDate,
                                                                                                                direction,
                                                                                                                state,
                                                                                                                users) {

}

Bus::Bus(const std::string &lineId, const std::string &stationId, int startingDate, int endDate,
               Direction direction, State state, const std::vector<std::shared_ptr<User>> &users, std::string type)
        : VehicleImpl(lineId, stationId, startingDate, endDate,
                      direction, state,
                      users, type) {}

Bus::Bus(const std::string &lineId, const std::string &stationId, int startingDate, int endDate,
               Direction direction, State state, const std::vector<std::shared_ptr<User>> &users, std::string vehiculeId, std::string type)
        : VehicleImpl(lineId, stationId, startingDate, endDate,
                      direction, state,
                      users, vehiculeId, type) {}

std::shared_ptr<Vehicle> Bus::update(const std::string &stationId, const State &state, int time) const {
    int endDate = _endDate;
    return std::make_shared<Bus>(_lineId, stationId, endDate, endDate += time, _direction, state, _users, _vehicleId, _type);
}
