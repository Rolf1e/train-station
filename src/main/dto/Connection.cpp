#include "Connection.hpp"

const BinaryStation &Connection::getNeighbours() const {
    return _neighbours;
}

Connection::Connection(const BinaryStation &neighbours) : _neighbours(neighbours) {}
