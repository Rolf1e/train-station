#ifndef TRAIN_STATION_CONNECTION_HPP
#define TRAIN_STATION_CONNECTION_HPP

#include <memory>
#include "../infra/BinaryStation.hpp"

class Connection {
private:
    BinaryStation _neighbours;

public:
    const BinaryStation &getNeighbours() const;

    Connection(const BinaryStation &neighbours);
};


#endif //TRAIN_STATION_CONNECTION_HPP
