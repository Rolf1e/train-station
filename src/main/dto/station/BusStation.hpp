#ifndef TRAIN_STATION_BUSSTATION_HPP
#define TRAIN_STATION_BUSSTATION_HPP

#include "../Connection.hpp"
#include "StationImpl.hpp"

class BusStation : public StationImpl {
public:

    BusStation(const std::string &id, const Connection &neighbours, const std::vector<std::shared_ptr<User>> &users);

};

#endif //TRAIN_STATION_BUSSTATION_HPP
