#include "StationImpl.hpp"

const std::string &StationImpl::getId() const {
    return _id;
}

const Connection &StationImpl::getNeighbours() const {
    return _neighbours;
}

const std::vector<std::shared_ptr<User>> &StationImpl::getUsers() const {
    return _users;
}

StationImpl::StationImpl(const std::string &id, const Connection &neighbours,
                         const std::vector<std::shared_ptr<User>> &users) : _id(id),
                                                                            _neighbours(
                                                                                    neighbours),
                                                                            _users(users) {}
