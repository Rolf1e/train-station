#ifndef TRAIN_STATION_STATION_HPP
#define TRAIN_STATION_STATION_HPP

#include "../User.hpp"
#include "../Connection.hpp"

#include <vector>

class Station {
public :

    virtual const Connection &getNeighbours() const = 0;

    virtual const std::string &getId() const = 0;
};


#endif //TRAIN_STATION_STATION_HPP
