#ifndef TRAIN_STATION_STATIONIMPL_HPP
#define TRAIN_STATION_STATIONIMPL_HPP

#include <memory>
#include "Station.hpp"
#include "../Connection.hpp"

class StationImpl : public Station {
private:
    std::string _id;
    Connection _neighbours;
    std::vector<std::shared_ptr<User>> _users;
protected:
    StationImpl(const std::string &id, const Connection &neighbours, const std::vector<std::shared_ptr<User>> &users);

public:
    const std::string &getId() const;

    const Connection &getNeighbours() const override;

    const std::vector<std::shared_ptr<User>> &getUsers() const;

};


#endif //TRAIN_STATION_STATIONIMPL_HPP
