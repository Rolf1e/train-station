#include "SubwayStation.hpp"

SubwayStation::SubwayStation(const std::string &id, const Connection &neighbours,
                             const std::vector<std::shared_ptr<User>> &users)
        : StationImpl(id, neighbours, users) {}
