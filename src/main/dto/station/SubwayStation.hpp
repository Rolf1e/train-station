#ifndef TRAIN_STATION_SUBWAYSTATION_HPP
#define TRAIN_STATION_SUBWAYSTATION_HPP

#include "../Connection.hpp"
#include "StationImpl.hpp"

class SubwayStation : public StationImpl {
public:

    SubwayStation(const std::string &id, const Connection &neighbours, const std::vector<std::shared_ptr<User>> &users);

};

#endif //TRAIN_STATION_SUBWAYSTATION_HPP
