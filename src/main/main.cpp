#include <iostream>
#include <ostream>
#include <string>
#include "dto/station/Station.hpp"
#include "dto/station/SubwayStation.hpp"
#include "dto/vehicle/Direction.hpp"
#include "dto/vehicle/State.hpp"
#include "dto/vehicle/Subway.hpp"
#include "infra/parser/JsonParser.hpp"
#include "infra/manager/LineManager.hpp"
#include "infra/manager/LineManagerBuilder.hpp"


int main(int argc, char** argv) {
	if(argc <= 1 ) {
		std::cout << argc << std::endl;
		std::cout << "Please give path to directory with yourkata.json" << std::endl;
		return 1;
	}

	std::string path = argv[1];

	auto json = JsonParser::getInterConnections(path +  "yourkata.json");
	auto lines = JsonParser::getNetworking(path + "yourkata.json");

	for( auto &tmp : json) {
		std::cout << tmp.getDuration() << std::endl;
	}


	LineManager manager(2000, lines);

	manager.load();

	manager.start();

	return 0;
}
