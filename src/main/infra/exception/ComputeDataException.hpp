#ifndef TRAIN_STATION_COMPUTEDATAEXCEPTION_HPP
#define TRAIN_STATION_COMPUTEDATAEXCEPTION_HPP


#include <exception>

struct ComputeDataException : public std::exception{
    const char *what() const noexcept override {
        return "State does not exist.";
    }
};


#endif //TRAIN_STATION_COMPUTEDATAEXCEPTION_HPP
