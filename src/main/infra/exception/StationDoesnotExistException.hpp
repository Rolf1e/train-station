//
// Created by rolfie on 6/29/20.
//

#ifndef TRAIN_STATION_STATIONDOESNOTEXISTEXCEPTION_HPP
#define TRAIN_STATION_STATIONDOESNOTEXISTEXCEPTION_HPP


#include <exception>

struct StationDoesnotExistException : public std::exception {
    const char *what() const noexcept override {
        return "Station does not exist.";
    }
};

#endif //TRAIN_STATION_STATIONDOESNOTEXISTEXCEPTION_HPP
