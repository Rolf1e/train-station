#ifndef TRAIN_STATION_STATEDOESNOTEXISTEXCEPTION_HPP
#define TRAIN_STATION_STATEDOESNOTEXISTEXCEPTION_HPP

#include <exception>

struct StateDoesnotExistException : public std::exception {
    const char *what() const noexcept override {
        return "State does not exist.";
    }
};


#endif //TRAIN_STATION_STATEDOESNOTEXISTEXCEPTION_HPP
