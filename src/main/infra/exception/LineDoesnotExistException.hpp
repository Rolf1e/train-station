#ifndef TRAIN_STATION_LINEDOESNOTEXISTEXCEPTION_HPP
#define TRAIN_STATION_LINEDOESNOTEXISTEXCEPTION_HPP


#include <exception>

struct LineDoesnotExistException : public std::exception {
    const char *what() const noexcept override {
        return "Line does not exist.";
    }
};


#endif //TRAIN_STATION_LINEDOESNOTEXISTEXCEPTION_HPP
