#ifndef TRAIN_STATION_VEHICLESTATEUPDATER_HPP 
#define TRAIN_STATION_VEHICLESTATEUPDATER_HPP


#include "../BinarySeeker.hpp"
#include "../../dto/vehicle/Vehicle.hpp"

class VehicleStateUpdater {

	private:
		static std::shared_ptr<Vehicle> update(const std::shared_ptr<Vehicle> &vehicle, const std::pair<std::string, int> &binary, const State &state) {	
			return vehicle->update(binary.first, state, binary.second);
		};

	public:
		static std::shared_ptr<Vehicle> stop(const std::shared_ptr<Vehicle> &vehicle, int flipDuration){
			std::pair<std::string, int> flip(vehicle->getStationId(), flipDuration);
			return update(vehicle, flip, STOP);
		};

		static std::shared_ptr<Vehicle> start(const std::shared_ptr<Vehicle> &vehicle, int subwayStopDuration) {
			//fillHourly(vehicle);
			std::pair<std::string, int> stop(vehicle->getStationId(), subwayStopDuration);
			return update(vehicle, stop, START);
		};
		static std::shared_ptr<Vehicle> running(const std::shared_ptr<Vehicle> &vehicle){
			std::pair<std::string, int> start(vehicle->getStationId(), 0);
			return update(vehicle, start, RUNNING);
		};

		static std::shared_ptr<Vehicle> pending(const std::shared_ptr<Vehicle> &vehicle, std::vector<std::shared_ptr<Line>> lines) {

			BinaryStation binary = BinarySeeker::searchBinaryInLines(lines, vehicle);

			if (vehicle->getDirection() == UP) {
				if (binary.getUp().first == "") {
					std::pair<std::string, int> stop(vehicle->getStationId(), 0);

					vehicle->setDirection(DOWN);

					return update(vehicle, stop, PENDING);
				} else {
					return update(vehicle, binary.getUp(), STOP);
				}
			} else {
				if (binary.getDown().first == "") {
					std::pair<std::string, int> stop(vehicle->getStationId(), 0);

					vehicle->setDirection(UP);

					return update(vehicle, stop, PENDING);
				} else {
					return update(vehicle, binary.getDown(), STOP);
				}
			}
		};

		static std::shared_ptr<Vehicle> flip(const std::shared_ptr<Vehicle> &vehicle, int subwayStopDuration) {
			std::pair<std::string, int> stop(vehicle->getStationId(), subwayStopDuration);
			return update(vehicle, stop, FLIP);
		};



};


#endif //TRAIN_STATION_VEHICLESTATEUPDATER_HPP

