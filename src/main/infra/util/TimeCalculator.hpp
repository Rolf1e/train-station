#ifndef TRAIN_STATION_TIMECALCULATOR_HPP
#define TRAIN_STATION_TIMECALCULATOR_HPP

#include <algorithm>
#include "../../dto/vehicle/Vehicle.hpp"
#include "../../dto/line/Line.hpp"


class TimeCalculator {
public:
    static std::vector<std::tuple<std::string, int>> totalRameCourse(const int &totalTime, const std::vector<std::shared_ptr<Line>> &_lines) {
        std::vector<std::tuple<std::string, int>> tuples;

        for (const auto &line : _lines) {
            auto vehicles = line->getVehicles();
            std::for_each(vehicles.cbegin(), vehicles.cend(), [&totalTime, &tuples](const std::shared_ptr<Vehicle> &v) {
                tuples.emplace_back(v->getId(), computeCourseDurationForVehicle(totalTime, v));
            });
        }

        return tuples;
    }

    static std::vector<std::tuple<std::string, int>> timeBetweenTrain(const std::vector<std::shared_ptr<Line>> &_lines) {
        std::vector<std::tuple<std::string, int>> tuples;

        for (const auto &line : _lines) {
            auto station = line->getStations();
            int timeBetweenTwoTrains = line->getVehicles()[1]->getEndDate();
            std::for_each(station.cbegin(), station.cend(), [timeBetweenTwoTrains,&tuples](const std::shared_ptr<Station> &s) {
                tuples.emplace_back(s->getId(), timeBetweenTwoTrains);
            });
        }

        return tuples;
    }

    static int computeCourseDurationForVehicle(const int &totalTime, const std::shared_ptr<Vehicle> &v) {
        return totalTime - v->getStartingDate();
    }
};

#endif //TRAIN_STATION_TIMECALCULATOR_HPP
