#ifndef TRAIN_STATION_ENUMCONVERTER_HPP
#define TRAIN_STATION_ENUMCONVERTER_HPP

#include<string>

class EnumConverter{
public:
    static std::string getStringState(int state) {
        switch(state) {
            case 0:
                return "STOP";
            case 1:
                return "START";
            case 2:
                return "RUNNING";
            case 3:
                return "FLIP";
            case 4:
                return "PENDING";
            default:
                return "UNKNOWN";
        }
    }
    static std::string getStringDirection(int direction) {
        switch(direction) {
            case 0:
                return "DOWN";
            case 1:
                return "UP";
            default:
                return "UNKNOWN";
        }
    }
};

#endif //TRAIN_STATION_ENUMCONVERTER_HPP
