//
// Created by arthur on 11/06/2020.
//

#include <iostream>
#include <fstream>

#include "JsonParser.hpp"
#include "../../dto/line/LineImpl.hpp"
#include "../../dto/line/SubwayLine.hpp"
#include "../../dto/vehicle/Subway.hpp"
#include "../../dto/station/SubwayStation.hpp"
#include "../../dto/InterConnection.hpp"
#include "../../dto/vehicle/Bus.hpp"
#include "../../dto/station/BusStation.hpp"
#include "../../dto/line/BusLine.hpp"

using json = nlohmann::json;


std::vector<std::shared_ptr<Line>> JsonParser::getNetworking(const std::string path) {

    json j = getJsonParse(path);

    std::vector<std::shared_ptr<Line>> lines;

    std::string subwayKey[3] = {"subway", "stations", "train number"};
    addLine<Subway, SubwayStation, SubwayLine>(j, lines, subwayKey);

    std::string busKey[3] = { "bus", "stops", "bus number" };
    addLine<Bus,BusStation, BusLine>(j, lines, busKey);

    return lines;
}


std::string JsonParser::getBusLine(std::string station, nlohmann::json busLine) {

    for (auto &line : busLine["bus"]["lines"] ) {

        std::string stationName = line["name"];

        for (auto &stop : line["stops"] ) {
            if (station == stop ) {
                return stationName;
            }
        }
    }
    return std::string();
}



std::vector<InterConnection> JsonParser::getInterConnections(const std::string path) {

    json j = getJsonParse(path);
    std::vector<InterConnection> interConnections;

    for (auto &line : j["interconnections"]) {

        if (line["bike"] == nullptr) {

            interConnections.emplace_back(
                    std::make_tuple(line["subway"]["line"], line["subway"]["station"]),
                    std::make_tuple(line["bus"]["line"], line["bus"]["stop"]),
                    calculateTimeInSecond(line["duration"])
            );

        }
    }

    for (auto &connection : j["subway"]["connections"]) {

        std::string line1 = connection["stations"][0];
        std::string line2 = connection["stations"][1];
        int numberOfRemoveChar = line1.find("_") + 1;
        line1 = "L" + line1.substr(numberOfRemoveChar);
        line2 = "L" + line2.substr(numberOfRemoveChar);

        interConnections.emplace_back(
                std::make_tuple(line1, connection["stations"][0]),
                std::make_tuple(line2, connection["stations"][1]),
                calculateTimeInSecond(connection["duration"])
        );

    }

    for (auto &connection : j["bus"]["connections"]) {

        interConnections.emplace_back(
                std::make_tuple(getBusLine(connection["stops"][0], j), connection["stops"][0]),
                std::make_tuple(getBusLine(connection["stops"][1], j), connection["stops"][1]),
                calculateTimeInSecond(connection["duration"])
        );

    }

    return interConnections;
}


// calculate Time
int JsonParser::calculateTimeInSecond(double time) {
    return ceil(time * 60);
}

int JsonParser::calculateTimeToWait(json j, json line, std::string jsonKey[]) {
    int fullTime = 0;
    int numberOfStop = 1;

    for (auto duration : line["durations"]) {
        fullTime += JsonParser::calculateTimeInSecond(duration);
        numberOfStop++;
    }

    int stopDuration = calculateTimeInSecond(j[jsonKey[0]]["stop duration"]);
    int flipDuration = calculateTimeInSecond(line["flip duration"]);
    int trainNumber = line[jsonKey[2]];

    fullTime = ((numberOfStop * stopDuration) + fullTime + flipDuration) * 2;

    return fullTime / trainNumber;

}


//Json Parse
nlohmann::json JsonParser::getJsonParse(const std::string filename) {
    std::ifstream f(filename);
    return json::parse(f);
}


//BinaryStation
BinaryStation JsonParser::createBinaryStation(nlohmann::json line, int currentStation, std::string jsonKey[]) {
    std::string stationUp, stationDown = "";
    int timeUp, timeDown = 0;

    if (currentStation != 0) {
        stationUp = line[jsonKey[1]][currentStation - 1];
        timeUp = calculateTimeInSecond(line["durations"][currentStation - 1]);
    }

    if (currentStation != int(line[jsonKey[1]].size()) - 1) {
        stationDown = line[jsonKey[1]][currentStation + 1];
        timeDown = calculateTimeInSecond(line["durations"][currentStation]);
    }

    return BinaryStation(stationUp, timeUp, stationDown, timeDown);
}



