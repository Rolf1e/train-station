//
// Created by arthur on 11/06/2020.
//

#ifndef TRAIN_STATION_JSONPARSER_HPP
#define TRAIN_STATION_JSONPARSER_HPP

#include <nlohmann/json.hpp>
#include "../../dto/line/LineImpl.hpp"
#include "../../dto/vehicle/State.hpp"
#include "../../dto/InterConnection.hpp"
#include "../../infra/manager/LineManager.hpp"

class JsonParser {

public:
    static std::vector<std::shared_ptr<Line>> getNetworking(const std::string path);
    static std::vector<InterConnection> getInterConnections(const std::string path);

private:

    // calculate Time
    static int calculateTimeInSecond(double time);

    static int calculateTimeToWait(nlohmann::json j, nlohmann::json line, std::string jsonKey[]);

    static std::string getBusLine(std::string station, nlohmann::json busLine);

    //Json Parse
    static nlohmann::json getJsonParse(const std::string filename);


    //BinaryStation
    static BinaryStation createBinaryStation(nlohmann::json line, int currentStation, std::string jsonKey[]);

    //intraConnections


    // Station
    template<typename T>
    static std::shared_ptr<T> createStation(nlohmann::json line, std::string jsonKey[], int currentStation) {

        std::vector<std::shared_ptr<User>> users = {};
        return std::make_shared<T>(line[jsonKey[1]][currentStation],
                                   Connection(createBinaryStation(line, currentStation, jsonKey)),
                                   users);
    };

    template<typename T>
    static std::vector<std::shared_ptr<Station>> createStations(nlohmann::json line, std::string jsonKey[]) {

        std::vector<std::shared_ptr<Station>> stations;

        for (int i = 0; i < int(line[jsonKey[1]].size()); ++i) {
            stations.push_back(createStation<T>(line, jsonKey, i));
        }

        return stations;
    };


    // vehicle
    template<typename T>
    static std::shared_ptr<T> createVehicle(int startingDate, int endDate, nlohmann::json line, std::string type, std::string typeStop) {

        std::vector<std::shared_ptr<User>> users = {};
        //return std::make_shared<T>(startingDate, endDate, UP, STOP, users, line["name"]);
        return std::make_shared<T>(line["name"], line[typeStop][0], startingDate, endDate, UP, STOP, users, type);
    };

    template<typename T>
    static std::vector<std::shared_ptr<Vehicle>>
    createVehicles(nlohmann::json j, nlohmann::json line, std::string *jsonKey) {

        std::vector<std::shared_ptr<Vehicle>> vehicles = {};
        int timeToWait = calculateTimeToWait(j, line, jsonKey);
        int numberOfVehicles = line[jsonKey[2]];

        if (jsonKey[0] == "bus") {
            numberOfVehicles = numberOfVehicles / 2;
        }

        for (int i = 0; i < numberOfVehicles; ++i) {

            int timeToWaitForCurrentVehicle = i * timeToWait;

            vehicles.push_back(
                    createVehicle<T>(timeToWaitForCurrentVehicle,
                                     timeToWaitForCurrentVehicle + calculateTimeInSecond(line["durations"][0]),
                                     line, jsonKey[0], jsonKey[1]));

            if (jsonKey[0] == "bus") {
                vehicles.push_back(
                        createVehicle<T>(timeToWaitForCurrentVehicle,
                                         timeToWaitForCurrentVehicle + calculateTimeInSecond(
                                                 line["durations"][line["durations"].size() - 1]), line, jsonKey[0], jsonKey[1]));
            }
        }

        return vehicles;
    }


    // Line
    template<typename T>
    static std::shared_ptr<T> createLine(nlohmann::json line, std::vector<std::shared_ptr<Vehicle>> vehicles , std::vector<std::shared_ptr<Station>> stations) {

        return std::make_shared<T>(line["name"], calculateTimeInSecond(line["flip duration"]), vehicles, stations);

    };

    template<typename T1, typename T2, typename T3>
    static void addLine(const nlohmann::json &j, std::vector<std::shared_ptr<Line>> &lines, std::string jsonKey[]) {

        for (const auto &line : j[jsonKey[0]]["lines"]) {
            lines.push_back(createLine<T3>(line, createVehicles<T1>(j, line, jsonKey), createStations<T2>(line, jsonKey)));
        }

    }


    LineManager getManager(const std::string path, const int &simulationDuration);
};


#endif //TRAIN_STATION_JSONPARSER_HPP
