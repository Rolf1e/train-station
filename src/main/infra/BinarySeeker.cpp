#include "BinarySeeker.hpp"
#include "exception/LineDoesnotExistException.hpp"
#include "exception/StationDoesnotExistException.hpp"

#include <algorithm>
#include <memory>

BinaryStation BinarySeeker::searchBinaryInLines(const std::vector<std::shared_ptr<Line>> &_lines,
                                                const std::shared_ptr<Vehicle> &currentVehicle) noexcept(false) {

    const auto &iterator = std::find_if(_lines.cbegin(), _lines.cend(),
                                        [&currentVehicle](const std::shared_ptr<Line> &line) {
                                            return line->getId() == currentVehicle->getLineId();
                                        });
    if (iterator != _lines.cend()) {
        return searchBinaryInStations(_lines.at(iterator - _lines.cbegin()), currentVehicle);
    }
    throw LineDoesnotExistException();
}

BinaryStation BinarySeeker::searchBinaryInStations(const std::shared_ptr<Line> &line,
                                                   const std::shared_ptr<Vehicle> &currentVehicle) noexcept(false) {
    auto stations = line->getStations();
    auto currentStationId = currentVehicle->getStationId();

    const auto &iterator = std::find_if(stations.cbegin(), stations.cend(),
                                        [&currentVehicle](const std::shared_ptr<Station> &station) {
                                            return station->getId() == currentVehicle->getStationId();
                                        });
    if (iterator != stations.cend()) {
        const auto &station = stations.at(iterator - stations.cbegin());
        return station->getNeighbours().getNeighbours();
    }
    throw StationDoesnotExistException();
}
