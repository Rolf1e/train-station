#include <iostream>
#include <algorithm>
#include <memory>
#include <utility>
#include <fstream>
#include <type_traits>

#include "LineManager.hpp"
#include "../../dto/line/Line.hpp"
#include "../../dto/Connection.hpp"
#include "../../dto/vehicle/State.hpp"
#include "../BinarySeeker.hpp"
#include "../util/TimeCalculator.hpp"
#include "../exception/StateDoesnotExistException.hpp"
#include "../exception/LineDoesnotExistException.hpp"
#include "../exception/ComputeDataException.hpp"
#include "../exception/StationDoesnotExistException.hpp"
#include "PythonManager.hpp"
#include "../util/VehicleStateUpdater.hpp"

LineManager::LineManager(int simulationDuration) {
	_simulationDuration = simulationDuration;
	_totalTime = 0;
	_subwayStopDuration = 0; // TODO
	_busStopDuration = 0; // TODO
	load();
}

LineManager::LineManager(int simulationDuration, std::vector<std::shared_ptr<Line>> &lines) {
	_simulationDuration = simulationDuration;
	_lines = lines;
	_subwayStopDuration = 0; // TODO
	_busStopDuration = 0; // TODO
	_totalTime = 0;
}

int LineManager::getSubwayStopDuration() const {
	return _subwayStopDuration;
}

int LineManager::getBusStopDuration() const {
	return _busStopDuration;
}

void LineManager::setSubwayStopDuration(int subwayStopDuration) {
	LineManager::_subwayStopDuration = subwayStopDuration;
}

void LineManager::setBusStopDuration(int busStopDuration) {
	LineManager::_busStopDuration = busStopDuration;
}

int LineManager::getTime() const {
	return _totalTime;
}

const std::vector<PythonData> &LineManager::getPythonData() const {
	return pythonData;
}

void LineManager::setTime(const int &time) {
	_totalTime = time;
}

int LineManager::calculateVehicleNumber(const std::string &type) const {
	int count = 0;
	std::for_each(_lines.cbegin(), _lines.cend(), [&count, &type](const std::shared_ptr<Line> &line) {
			std::vector<std::shared_ptr<Vehicle>> vehicles = line->getVehicles();
			std::for_each(vehicles.cbegin(), vehicles.cend(), [&count, &line, &type](const std::shared_ptr<Vehicle> &vehicle) {
					if(vehicle->getType() == type) {
					count +=1;
					}
					});	
			});
	return count;
}

void LineManager::loadHourly() {
	for(auto &line: _lines)
	{
		for(auto &station : line->getStations()) {
			std::vector<std::tuple<std::string,int>> currentRame;
			_hourly.push_back({station->getId(),currentRame});
		}
	}
}

void LineManager::fillHourly(const std::shared_ptr<Vehicle> &vehicle)
{
	for(auto &station : _hourly ){
		if(std::get<0>(station) == vehicle->getStationId())
		{
			std::get<1>(station).push_back({vehicle->getId(),vehicle->getEndDate()});
		}
	}
}

std::vector<std::tuple<std::string,std::vector<std::tuple<std::string,int>>>> LineManager::getHourly() const {
	return _hourly;
}


void LineManager::load() {
	loadHourly();
	for (auto &line : _lines) {
		for (const auto &vehicle : line->getVehicles()) {
			_queue.push(vehicle);
		}
	}
}

void LineManager::start() {
	while (!_queue.empty() and _totalTime <= _simulationDuration) {
		auto currentVehicle = _queue.top();
		std::cout << "t = " << _totalTime << " => " << *currentVehicle;

		_queue.pop();

		setTime(currentVehicle->getStartingDate());

		try {
			switch (currentVehicle->getState()) {
				case START: {
						    currentVehicle = VehicleStateUpdater::running(currentVehicle);
						    break;
					    }
				case RUNNING: {
						      currentVehicle = VehicleStateUpdater::pending(currentVehicle, _lines);
						      break;
					      }
				case PENDING: {
						      currentVehicle = VehicleStateUpdater::flip(currentVehicle, getSubwayStopDuration());
						      break;
					      }
				case FLIP: {
						   currentVehicle = VehicleStateUpdater::stop(currentVehicle, getFlipDuration(currentVehicle));
						   break;
					   }
				case STOP: {

						   fillHourly(currentVehicle);
						   currentVehicle = VehicleStateUpdater::start(currentVehicle, getSubwayStopDuration());
						   break;
					   }
				default:
					   throw StateDoesnotExistException();
			}
			_queue.push(currentVehicle);
		} catch (StationDoesnotExistException &e) {
			std::cout << e.what() << std::endl;
		} catch (LineDoesnotExistException &e) {
			std::cout << e.what() << std::endl;
		}
	}

	auto totalTimeCourse = TimeCalculator::totalRameCourse(_totalTime, _lines);
	auto timeBetweenTrain = TimeCalculator::timeBetweenTrain(_lines);
	auto hourly = getHourly();

	PythonManager::addData(PythonData(calculateVehicleNumber("subway"), totalTimeCourse, timeBetweenTrain, hourly));
	PythonManager::addData(PythonData(calculateVehicleNumber("bus"), totalTimeCourse, timeBetweenTrain, hourly));
	pythonData = PythonManager::getData();
}

int LineManager::getFlipDuration(const std::shared_ptr<Vehicle> &currentVehicle) const {
	auto currentLineId = currentVehicle->getLineId();
	const auto &iterator = std::find_if(_lines.cbegin(), _lines.cend(),
			[&currentLineId](const std::shared_ptr<Line> &line) {
			return line->getId() == currentLineId;
			});
	if (iterator != _lines.cend()) {
		return _lines.at(iterator - _lines.cbegin())->getFlipDuration();
	}
	throw ComputeDataException();
}

std::ostream &LineManager::operator<<(std::ostream &os) {
	os << "Nombre de Rames : " << calculateVehicleNumber("subway") << std::endl;
	os << "Nombre de Bus : " << calculateVehicleNumber("bus") << std::endl;
	os << "Temps de parcours totale de la ligne par une rame";
	for (const auto &line : _lines) {
		auto vehicles = line->getVehicles();
		std::for_each(vehicles.cbegin(), vehicles.cend(), [this, &os](const std::shared_ptr<Vehicle> &v) {
				os << "Vehicle " << v->getId() << " : " << TimeCalculator::computeCourseDurationForVehicle(_totalTime, v)
				<< std::endl;
				});
	}
	return os;
}

