//
// Created by arthur on 01/07/2020.
//

#ifndef TRAIN_STATION_LINEMANAGERBUILDER_HPP
#define TRAIN_STATION_LINEMANAGERBUILDER_HPP

#include "LineManager.hpp"

class LineManagerBuilder {

public:
    static LineManager getLineManager(const std::string &path, int simulationDuration);


};


#endif //TRAIN_STATION_LINEMANAGERBUILDER_HPP
