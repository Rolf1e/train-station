#ifndef TRAIN_STATION_LINEMANAGER_HPP
#define TRAIN_STATION_LINEMANAGER_HPP


#include <memory>
#include <queue>
#include <ostream>
#include <vector>
#include "../../dto/vehicle/Vehicle.hpp"
#include "../../dto/line/Line.hpp"
#include "../../dto/vehicle/Subway.hpp"

struct VehicleComparator {
	bool operator()(const std::shared_ptr<Vehicle> &v1, const std::shared_ptr<Vehicle> &v2) {
		return *v1 < v2;
	}
};

typedef std::priority_queue<std::shared_ptr<Vehicle>, std::vector<std::shared_ptr<Vehicle>>, VehicleComparator> VehicleQueue;
typedef std::tuple<int, std::vector<std::tuple<std::string, int>>, std::vector<std::tuple<std::string, int>> , std::vector<std::tuple<std::string,std::vector<std::tuple<std::string,int>>>>> PythonData;
typedef std::tuple<std::string,std::vector<std::tuple<std::string,int>>> Hourly;


class LineManager {
	private:
		VehicleQueue _queue;
		std::vector<std::shared_ptr<Line>> _lines;
		std::vector<Hourly> _hourly;
		int _simulationDuration;
		int _totalTime;
		int _subwayStopDuration;
		int _busStopDuration;
		std::vector<PythonData> pythonData;

		int getFlipDuration(const std::shared_ptr<Vehicle> &currentVehicle) const noexcept(false);

	public :

		LineManager(int simulationDuration);

		LineManager(int simulationDuration, std::vector<std::shared_ptr<Line>> &lines);
		
		std::vector<Hourly> getHourly() const;

		int getSubwayStopDuration() const;

		int getBusStopDuration() const;

		int getTime() const;

		void setTime(const int &time);

		void setSubwayStopDuration(int subwayStopDuration);
    		
		void loadHourly();

		void fillHourly(const std::shared_ptr<Vehicle> &vehicle );

		void setBusStopDuration(int busStopDuration);

		int calculateVehicleNumber(const std::string &type) const;

		void start();

		void load();

		std::ostream &operator<<(std::ostream &os);

		const std::vector<PythonData> &getPythonData() const;


};


#endif //TRAIN_STATION_LINEMANAGER_HPP

