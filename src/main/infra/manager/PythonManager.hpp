#ifndef TRAIN_STATION_PYTHONMANAGER_HPP
#define TRAIN_STATION_PYTHONMANAGER_HPP

#include <vector>
#include <string>
#include <tuple>

typedef std::tuple<int, std::vector<std::tuple<std::string, int>>, std::vector<std::tuple<std::string, int>> , std::vector<std::tuple<std::string,std::vector<std::tuple<std::string,int>>>>> PythonData;

class PythonManager{
	private:
		static std::vector<PythonData> datas;

	public: 
		static void addData(PythonData pythonData) {
			datas.push_back(pythonData);
		};

		static std::vector<PythonData> getData() {
			return datas;
		};

};


#endif //TRAIN_STATION_PYTHONMANAGER_HPP
