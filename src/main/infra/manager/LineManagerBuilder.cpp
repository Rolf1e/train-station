//
// Created by arthur on 01/07/2020.
//

#include "LineManagerBuilder.hpp"
#include "../parser/JsonParser.hpp"


LineManager LineManagerBuilder::getLineManager(const std::string &path, int simulationDuration) {
    auto network = JsonParser::getNetworking(path);
    return LineManager(simulationDuration, network);
}
