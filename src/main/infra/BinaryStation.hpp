#ifndef TRAIN_STATION_BINARYSTATION_HPP
#define TRAIN_STATION_BINARYSTATION_HPP

#include <utility>
#include <memory>
#include <string>
class BinaryStation {
private:
    std::pair<std::string, int> _up;
    std::pair<std::string, int> _down;

public:
    BinaryStation(const std::string&stationUp, const int &timeUp,
                  const std::string &stationDown, const int &timeDown);

    BinaryStation(const std::pair<std::string, int> &up,
                  const std::pair<std::string, int> &down);

    const int &getTimeUp() const;

    const std::string &getStationUp() const;

    const int &getTimeDown() const;

    const std::string &getStationDown() const;

    const std::pair<std::string, int> &getUp() const;

    const std::pair<std::string, int> &getDown() const;
};


#endif //TRAIN_STATION_BINARYSTATION_HPP
