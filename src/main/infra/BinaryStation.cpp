//
// Created by deneudtlucas on 11/06/2020.
//

#include "BinaryStation.hpp"

BinaryStation::BinaryStation(const std::string &stationUp, const int &timeUp,
                             const std::string &stationDown, const int &timeDown) : _up(stationUp, timeUp),
                                                                                                 _down(stationDown,
                                                                                                       timeDown) {}

BinaryStation::BinaryStation(const std::pair<std::string, int> &up,
                             const std::pair<std::string, int> &down) : _up(up), _down(down) {}

const int &BinaryStation::getTimeUp() const {
    return _up.second;
}

const std::string &BinaryStation::getStationUp() const {
    return _up.first;
}

const int &BinaryStation::getTimeDown() const {
    return _down.second;
}

const std::string &BinaryStation::getStationDown() const {
    return _up.first;
}

const std::pair<std::string, int> &BinaryStation::getUp() const {
    return _up;
}

const std::pair<std::string, int> &BinaryStation::getDown() const {
    return _down;
}
