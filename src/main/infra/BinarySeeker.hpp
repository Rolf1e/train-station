#ifndef TRAIN_STATION_BINARYSEEKER_HPP
#define TRAIN_STATION_BINARYSEEKER_HPP


#include "BinaryStation.hpp"
#include "../dto/vehicle/Vehicle.hpp"
#include "../dto/line/Line.hpp"
#include "exception/StateDoesnotExistException.hpp"

class BinarySeeker {
public:
    static BinaryStation searchBinaryInLines(const std::vector<std::shared_ptr<Line>> &_lines,
                                             const std::shared_ptr<Vehicle> &currentVehicle) noexcept(false);

    static BinaryStation searchBinaryInStations(const std::shared_ptr<Line> &line,
                                                const std::shared_ptr<Vehicle> &currentVehicle) noexcept(false);
};


#endif //TRAIN_STATION_BINARYSEEKER_HPP
