cmake_minimum_required(VERSION 3.0)
project(train-station)
set(CMAKE_CXX_STANDARD 14)
set(CMAKE_CXX_FLAGS "${CMAKE_CXX_FLAGS} -std=c++14 -Wall -Wextra")

find_package(PkgConfig REQUIRED)
pkg_check_modules(PKG_CPPUTEST REQUIRED cpputest)
include_directories(${PKG_CPPUTEST_INCLUDE_DIRS})

#  main programme
add_executable(
        main_train_station.out
        main/main.cpp

        main/dto/Connection.hpp
        main/dto/Connection.cpp
        main/dto/InterConnection.hpp
        main/dto/InterConnection.cpp
        main/dto/User.hpp

        main/dto/line/Line.hpp
        main/dto/line/LineImpl.hpp
        main/dto/line/LineImpl.cpp
        main/dto/line/SubwayLine.hpp
        main/dto/line/SubwayLine.cpp
        main/dto/line/BusLine.hpp
        main/dto/line/BusLine.cpp

        main/dto/station/Station.hpp
        main/dto/station/StationImpl.hpp
        main/dto/station/StationImpl.cpp
        main/dto/station/SubwayStation.hpp
        main/dto/station/SubwayStation.cpp
        main/dto/station/BusStation.hpp
        main/dto/station/BusStation.cpp

        main/dto/vehicle/Direction.hpp
        main/dto/vehicle/State.hpp
        main/dto/vehicle/Subway.hpp
        main/dto/vehicle/Subway.cpp
        main/dto/vehicle/Bus.hpp
        main/dto/vehicle/Bus.cpp
        main/dto/vehicle/Vehicle.hpp
        main/dto/vehicle/Vehicle.cpp
        main/dto/vehicle/VehicleImpl.hpp
        main/dto/vehicle/VehicleImpl.cpp

        main/infra/BinaryStation.hpp
        main/infra/BinaryStation.cpp
        main/infra/BinarySeeker.hpp
        main/infra/BinarySeeker.cpp

        main/infra/exception/ComputeDataException.hpp
        main/infra/exception/LineDoesnotExistException.hpp
        main/infra/exception/StateDoesnotExistException.hpp
        main/infra/exception/StationDoesnotExistException.hpp

        main/infra/manager/PythonManager.hpp
        main/infra/manager/PythonManager.cpp
        main/infra/manager/LineManager.hpp
        main/infra/manager/LineManager.cpp
        main/infra/manager/LineManagerBuilder.cpp
        main/infra/manager/LineManagerBuilder.hpp

        main/infra/parser/JsonParser.hpp
        main/infra/parser/JsonParser.cpp

        main/infra/util/EnumConverter.hpp
        main/infra/util/VehicleStateUpdater.hpp
        main/infra/util/TimeCalculator.hpp
)
target_link_libraries(main_train_station.out)


pkg_check_modules(PKG_CPPUTEST REQUIRED cpputest)
include_directories(${PKG_CPPUTEST_INCLUDE_DIRS})
add_executable(main_test.out tests/main_tests.cpp

        main/dto/Connection.hpp
        main/dto/Connection.cpp
        main/dto/InterConnection.hpp
        main/dto/InterConnection.cpp
        main/dto/User.hpp

        main/dto/line/Line.hpp
        main/dto/line/LineImpl.hpp
        main/dto/line/LineImpl.cpp
        main/dto/line/SubwayLine.hpp
        main/dto/line/SubwayLine.cpp
        main/dto/line/BusLine.hpp
        main/dto/line/BusLine.cpp

        main/dto/station/Station.hpp
        main/dto/station/StationImpl.hpp
        main/dto/station/StationImpl.cpp
        main/dto/station/SubwayStation.hpp
        main/dto/station/SubwayStation.cpp
        main/dto/station/BusStation.hpp
        main/dto/station/BusStation.cpp

        main/dto/vehicle/Direction.hpp
        main/dto/vehicle/State.hpp
        main/dto/vehicle/Subway.hpp
        main/dto/vehicle/Subway.cpp
        main/dto/vehicle/Bus.hpp
        main/dto/vehicle/Bus.cpp
        main/dto/vehicle/Vehicle.hpp
        main/dto/vehicle/Vehicle.cpp
        main/dto/vehicle/VehicleImpl.hpp
        main/dto/vehicle/VehicleImpl.cpp

        main/infra/BinaryStation.hpp
        main/infra/BinaryStation.cpp
        main/infra/BinarySeeker.hpp
        main/infra/BinarySeeker.cpp

        main/infra/exception/ComputeDataException.hpp
        main/infra/exception/LineDoesnotExistException.hpp
        main/infra/exception/StateDoesnotExistException.hpp
        main/infra/exception/StationDoesnotExistException.hpp

        main/infra/manager/PythonManager.hpp
        main/infra/manager/PythonManager.cpp
        main/infra/manager/LineManager.hpp
        main/infra/manager/LineManager.cpp
        main/infra/manager/LineManagerBuilder.cpp
        main/infra/manager/LineManagerBuilder.hpp

        main/infra/parser/JsonParser.hpp
        main/infra/parser/JsonParser.cpp

        main/infra/util/EnumConverter.hpp
        main/infra/util/VehicleStateUpdater.hpp
        main/infra/util/TimeCalculator.hpp

        tests/dto/line/line_test.cpp
        tests/dto/station/station_test.cpp
        tests/dto/vehicle/vehicle_test.cpp

        tests/infra/binaryseeker_test.cpp

        tests/infra/manager/linemanager_test.cpp
        tests/infra/parser/jsonparser_test.cpp
        tests/infra/util/enumconverter_test.cpp
        tests/infra/util/vehiclestateupdater_test.cpp

        )
target_link_libraries(main_test.out ${PKG_GTK_LIBRARIES} ${PKG_CPPUTEST_LIBRARIES})


find_package(Doxygen)
option(BUILD_DOCUMENTATION "Create and install the HTML based API
documentation (requires Doxygen)" ${DOXYGEN_FOUND})

if (BUILD_DOCUMENTATION)
    if (NOT DOXYGEN_FOUND)
        message(FATAL_ERROR "Doxygen is needed to build the documentation.")
    endif ()

    set(doxyfile_in ${CMAKE_CURRENT_SOURCE_DIR}/../docs/Doxyfile.in)
    set(doxyfile ${CMAKE_CURRENT_BINARY_DIR}/../docs/Doxyfile)

    configure_file(${PROJECT_SOURCE_DIR}/../docs/Doxyfile.in ${PROJECT_SOURCE_DIR}/../docs/Doxyfile @ONLY)


    message("Doxygen build started.")

    add_custom_target(doc ALL
            COMMAND ${DOXYGEN_EXECUTABLE} ${doxyfile_in}
            WORKING_DIRECTORY ${CMAKE_CURRENT_SOURCE_DIR}
            COMMENT "Generating API documentation with Doxygen"
            VERBATIM)

    #    install(DIRECTORY ${CMAKE_CURRENT_BINARY_DIR}/html DESTINATION     share/doc)
endif ()
